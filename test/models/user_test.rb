require 'test_helper'

class UserTest < ActiveSupport::TestCase
  
  setup do
  	@user = User.create()
  	@user.save
  	@course = Course.create()
    @course.save
    @question = @user.questions.create!(course_id: @course.id, title: 'h', description: 'h')
    @question.save
    @answer = @user.answers.create!(question_id: @question.id)
    @answer.save
  end
  
  test "get reputation" do
  	assert_equal 0, @user.get_reputation
  end
  
  test "get upvoted reputation" do
    vote = Vote.create!(user_id: @user.id, answer_id: @answer.id)
    vote.save
  	assert_not_equal 0, @user.votes.count
  end
  
  test "get upvoted reputation calulation" do
    vote = Vote.create!(user_id: @user.id, answer_id: @answer.id)
    vote.up
    vote.save
  	assert_equal 1, @user.get_reputation
  end
  	
  test "get downvoted answer reputation calulation" do
    vote = Vote.create!(user_id: @user.id, answer_id: @answer.id)
    vote.down
    vote.save
  	assert_equal -1, @user.get_reputation
  end
  
  test "get downvoted question reputation calulation" do
    vote = Vote.create!(user_id: @user.id, question_id: @question.id)
    vote.down
    vote.save
  	assert_equal -1, @user.get_reputation
  end
end

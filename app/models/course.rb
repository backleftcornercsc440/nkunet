class Course < ApplicationRecord
    has_and_belongs_to_many :users
    has_many :questions
    
    def full_name
        return name + " " + code
    end
    
    def get_students
        students = []
        users.each do |user|
            if user.is_student?
                students << user
            end
        end
        return students
    end
    
    def get_teachers
        teachers = []
        users.each do |user|
            if user.is_faculty?
                teachers << user
            end
        end
        return teachers
    end
end

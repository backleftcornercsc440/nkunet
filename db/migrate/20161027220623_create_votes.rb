class CreateVotes < ActiveRecord::Migration[5.0]
  def change
    if ActiveRecord::Base.connection.table_exists? :votes
      drop_table :votes
    end
    create_table :votes do |t|
      t.integer :topic_id
      t.integer :user_id
      t.datetime :created_at
      t.datetime :updated_at

      t.timestamps
    end
  end
end

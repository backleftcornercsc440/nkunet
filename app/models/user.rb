class User < ApplicationRecord
    has_and_belongs_to_many :courses
    has_many :votes, dependent: :destroy
    has_many :answers
    has_many :questions
    has_secure_password
    validates :name,  presence: true, length: { maximum: 50 }
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
    attr_accessor :remember_token

    #calculates user reputation
    def get_reputation
    	rep = 0
    	answers.each do |a|
    		a.votes.each do |v|
    			status = v.status
    			if status == 1
    				rep = rep + 1
    			else
    				rep = rep - 1
    			end
    		end
    	end
    	questions.each do |q|
    		q.votes.each do |v|
    			status = v.status
    			if status == 1
    				rep = rep + 1
    			else
    				rep = rep - 1
    			end
    		end
    	end
    	return rep
    end
    
    def is_admin?
        return acct_type == 0
    end
    
    def is_registrar?
       return acct_type == 1 
    end
    
    def is_faculty?
       return acct_type == 2
    end
    
    def is_user?
       return !is_admin? && !is_registrar?
    end
    
    def is_student?
        return !is_admin? && !is_registrar? && !is_faculty?
    end
    
    # Returns the hash digest of the given string.
    def User.new_token
        SecureRandom.urlsafe_base64
    end

    # Remembers a user in the database for use in persistent sessions.
    def remember
        self.remember_token = User.new_token
        update_attribute(:remember_digest, User.digest(remember_token))
    end

    
    # Returns the hash digest of the given string.
    def self.digest(string)
        cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST : BCrypt::Engine.cost
        BCrypt::Password.create(string, cost: cost)
    end
    
    # Returns a random token.
    def self.new_token
        SecureRandom.urlsafe_base64
    end
    
    # Returns true if the given token matches the digest.
    def authenticated?(remember_token)
        return false if remember_digest.nil?
        BCrypt::Password.new(remember_digest).is_password?(remember_token)
    end
    
    # Forgets a user.
    def forget
        update_attribute(:remember_digest, nil)
    end
    
    def upvoted_question?(question)
        @vote = question.get_user_vote(id)
        if @vote
           return @vote.get_status() == 1 
        else
           return false
        end
    end
    
    def downvoted_question?(question)
        @vote = question.get_user_vote(id)
        if @vote
           return @vote.get_status() == 0 
        else
           return false
        end
    end
    
    def upvoted_answer?(answer)
        @vote = answer.get_user_vote(id)
        if @vote
           return @vote.get_status() == 1 
        else
           return false
        end
    end
    
    def downvoted_answer?(answer)
        @vote = answer.get_user_vote(id)
        if @vote
           return @vote.get_status() == 0 
        else
           return false
        end
    end
    
    def account_type
        if is_admin?
           return "Administrator" 
        end
        if is_registrar?
           return "Registrar" 
        end
        if is_faculty?
           return "Faculty" 
        end
        return "Student"
    end
    
    def self.csv_upload(filename)
        require 'csv'
        begin
            CSV.foreach(filename, :headers => true) do |row|
                accountType = row['acct_type'].downcase
                if accountType == "faculty"
                    acct_type = 2
                else
                    acct_type = 3
                end
                name = row['name']
                username = row['username']
                password = row['password']
                phone_number = row['phone_number']
                email = row['email']
                user = User.create(:acct_type => acct_type,
                :reputation => 0,
                :name => name,
                :username => username,
                :password => password,
                :phone => phone_number,
                :email => email)
                courseIndex = 1
                course_id = row['course' + courseIndex.to_s()]
                while (course_id && course_id != ""  && course_id =~ /^[0-9]+$/ && course_id.to_i() > -1 && Course.find(course_id.to_i()))
                    user.courses << Course.find(course_id.to_i())
                    courseIndex = courseIndex + 1
                    course_id = row['course' + courseIndex.to_s()]
                end
                user.save
            end 
        rescue Exception => e
            puts e
            return false
        end
        return true
    end
end

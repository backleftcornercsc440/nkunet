class Vote < ApplicationRecord
    belongs_to :user
    
    validate :question_xor_answer

    #UPVOTE = 1
    #DOWNVOTE = 0
    @status

    #Getter
    def get_status
    	return read_attribute(:status)
    end
    
    #Setter
    def status=(new_status)
        @status = new_status
    end
    
    #Upvote Method
    def up
        @status=1
        write_attribute(:status, 1)
        save
        return @vote
    end
    
    #Downvote Method
    def down
        @status=0
        write_attribute(:status, 0)
        save
        return @vote
    end
    
    private

    def question_xor_answer
      unless answer_id.blank? ^ question_id.blank?
        errors.add(:base, "Specify a question or answer, not both")
      end
    end

end

Rails.application.routes.draw do
  get 'welcome/index'
  resources :questions, only: [:index]
  get '/users/import', to: 'users#import'
  post '/users/import', to: 'users#import_file'
  resources :users
  resources :courses do
    resources :questions, only: [:show, :new, :edit, :create, :update, :destroy, :upvote, :downvote] do
      member do
        post 'upvote'
        post 'downvote'
      end
      resources :answers do
        member do
          post 'upvote'
          post 'downvote'
        end
      end
    end
  end
  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  get '/logout', to: 'sessions#destroy'
  resources :welcome, :path => '/'
end

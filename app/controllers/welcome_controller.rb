class WelcomeController < ApplicationController
  def index
    @questions = Question.where('created_at > ?', 5.days.ago)
    @questions = @questions.sort_by { |question| question.get_score }.reverse
    @questions = @questions.first(5)
  end
end

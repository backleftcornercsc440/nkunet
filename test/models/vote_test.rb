require 'minitest/autorun'

class VoteTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  setup do
    @user = User.create()
  	@user.save
  	@course = Course.create()
    @course.save
    @question = @user.questions.create!(course_id: @course.id, title: 'h', description: 'h')
    @question.save
    @answer = @user.answers.create!(question_id: @question.id)
    @answer.save
  	@vote = Vote.create!(user_id: @user.id, question_id: @question.id)
  end

  test "get status of vote" do
  	assert_equal nil, @vote.get_status
  end
  
  test "upvote" do
    @vote.up()
    assert_equal 1, @vote.get_status
  end

  test "downvote" do
    @vote.down()
    assert_equal 0, @vote.get_status
  end
  
  test 'valid vote' do
    assert @vote.save
  end

  test 'vote db status' do
    @vote.up()
    assert_equal 1, @vote.status
  end
  
  test "vote without question or answer" do
    vote = Vote.create(user_id: @user.id)
    assert !vote.valid?
  end
  
  test "vote search" do
    assert Vote.where(user_id: @user.id).take == @vote
  end
  
  
end

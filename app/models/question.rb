class Question < ApplicationRecord
  belongs_to :user
  belongs_to :course
  has_many   :votes
  has_many :answers, dependent: :destroy
  
  #Validation
  validates :title, presence: true
  validates :description, presence: true
  
  def up_vote(user_id)
    @vote = get_user_vote(user_id)
    if @vote
      @vote.destroy()
    else
      @vote = votes.create(user_id: user_id)
  	  @vote.up
    end
  end
  
  def down_vote(user_id)
  	@vote = get_user_vote(user_id)
    if @vote
      @vote.destroy()
    else
      @vote = votes.create(user_id: user_id)
  	  @vote.down
    end
  end
  
  def get_user_vote(user_id)
    #loop through existing votes and make sure user has not previously voted
    votes.each do |v|
      if v.user_id == user_id
        @vote = v
      end
    end
    return @vote
  end

  def get_score
  	score = 0
  	votes.each do |v|
  		if v.status == 1
  			score = score + 1
  		else
  			score = score - 1
  		end
  	end
  	score
  end
  
  def self.search(title, keyword, course_id, user_id)
    @results = self.all
    if title && title != ""
      @results = @results.where("title LIKE :title", title: "%#{title}%")
    end
    if keyword && keyword != ""
      @results = @results.where("description LIKE :keyword", keyword: "%#{keyword}%")
    end
    if course_id && course_id != ""  && course_id =~ /^[0-9]+$/ && course_id.to_i() >-1 && Course.find(course_id.to_i())
      @results = @results.where("course_id = ?", course_id)
    end
    if user_id && user_id != "" && user_id =~ /^[0-9]+$/ && user_id.to_i() >-1 && User.find(user_id.to_i())
      @results = @results.where("user_id = ?", user_id)
    end
    return @results
  end
  
end

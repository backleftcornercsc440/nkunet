json.array!(@users) do |user|
  json.extract! user, :id, :acct_type, :reputation, :name, :username, :password, :phone, :email
  json.url user_url(user, format: :json)
end

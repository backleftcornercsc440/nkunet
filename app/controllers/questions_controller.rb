class QuestionsController < ApplicationController
  before_action :set_question, only: [:show, :edit, :update, :destroy]

  # GET /questions
  # GET /questions.json
  def index
    @questions = Question.search(params[:title],params[:keyword],params[:course],params[:poster])
  end

  # GET /questions/1
  # GET /questions/1.json
  def show
  end

  # GET /questions/new
  def new
    if logged_in?
      @question = Question.new
    else
      redirect_to :back
    end
    
  end

  # GET /questions/1/edit
  def edit
    if logged_in? && (@question.user == current_user ||  current_user.is_admin?) 
    
    else
      redirect_to :back
    end
  end

  # POST /questions
  # POST /questions.json
  def create
    
    if logged_in?
      
      @course = Course.find(params[:course_id])
      @question = @course.questions.create(question_params)
    
      respond_to do |format|
        @question.user_id = current_user.id
        if @question.save
          @question.up_vote(current_user.id)
          format.html { redirect_to course_question_path(@question.course,@question), notice: 'Question was successfully created.' }
          format.json { render :show, status: :created, location: @question }
        else
          format.html { render :new }
          format.json { render json: @question.errors, status: :unprocessable_entity }
        end
      end
      
    else
      flash[:danger] = 'You must be logged in to post questions!' # Not quite right!
      redirect_to :back
    end
    
    
  end
  
  # POST /questions/X/upvote
  def upvote()
    @course = Course.find(params[:course_id])
    @question = @course.questions.find(params[:id])
    if logged_in?
      @question.up_vote(current_user.id)
    else
      flash[:danger] = 'You must be logged in to upvote questions!' # Not quite right!
    end
    redirect_to(course_question_path(@question.course,@question))
  end
  
  # POST /questions/X/downvote
  def downvote()
    @course = Course.find(params[:course_id])
    @question = @course.questions.find(params[:id])
    if logged_in?
      @question.down_vote(current_user.id)
    else
      flash[:danger] = 'You must be logged in to downvote questions!' # Not quite right!
    end
    redirect_to(course_question_path(@question.course,@question))
  end

  # PATCH/PUT /questions/1
  # PATCH/PUT /questions/1.json
  def update
    if logged_in? && (@question.user == current_user ||  current_user.is_admin?) 
      respond_to do |format|
        if @question.update(question_params)
          format.html { redirect_to course_question_path(@question.course,@question), notice: 'Question was successfully updated.' }
          format.json { render :show, status: :ok, location: @question }
        else
          format.html { render :edit }
          format.json { render json: @question.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to :back
    end
    
  end

  # DELETE /questions/1
  # DELETE /questions/1.json
  def destroy
    if logged_in? && (@question.user == current_user ||  current_user.is_admin?) 
      @course = Course.find(params[:course_id])
      @question = @course.questions.find(params[:id])
      @question.destroy
      respond_to do |format|
        format.html { redirect_to course_path(@course), notice: 'Question was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      redirect_to :back
    end
    
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_question
      @question = Course.find(params[:course_id]).questions.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def question_params
      params.require(:question).permit(:title, :description, :user_id, :course_id)
    end
end

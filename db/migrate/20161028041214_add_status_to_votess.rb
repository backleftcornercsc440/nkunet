class AddStatusToVotess < ActiveRecord::Migration[5.0]
  def change
    add_column :votes, :status, :integer
  end
end

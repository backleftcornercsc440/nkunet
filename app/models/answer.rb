class Answer < ApplicationRecord
  belongs_to :question
  belongs_to :user
  has_many   :votes, dependent: :destroy

  def up_vote(user_id)
    @vote = get_user_vote(user_id)
    if @vote
      @vote.destroy()
    else
      @vote = votes.create(user_id: user_id)
  	  @vote.up
    end
  end
  
  def down_vote (user_id)
    @vote = get_user_vote(user_id)
    if @vote
      @vote.destroy()
    else
      @vote = votes.create(user_id: user_id)
      @vote.down
    end
  end
  
  def get_user_vote(user_id)
    #loop through existing votes and make sure user has not previously voted
    votes.each do |v|
      if v.user_id == user_id
        @vote = v
      end
    end
    return @vote
  end

  def get_score
  	score = 0
  	votes.each do |v|
  		if v.status == 1
  			score = score + 1
  		else
  			score = score - 1
  		end
  	end
  	score
  end
  
end

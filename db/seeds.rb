# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


User.create(:acct_type => 0,
            :reputation => 10,
            :name => 'admin',
            :username => 'admin',
            :password => 'password',
            :phone => '8599162437',
            :email => 'admin@gmail.com',
            :created_at => 'October 20',
            :updated_at => 'October 20')

# create a registrar
User.create(:acct_type => 1,
            :reputation => 0,
            :name => 'reg1',
            :username => 'reg1',
            :password => 'reg1',
            :phone => '123123123',
            :email => 'reg1@nku-net.edu',
            :created_at => 'December 1',
            :updated_at => 'December 1')

# create some dummy users
# email is user#@nku-net.edu
# pass is user#
User.create(:acct_type => 3,
            :reputation => 0,
            :name => 'user1',
            :username => 'user1',
            :password => 'user1',
            :phone => '123123123',
            :email => 'user1@nku-net.edu',
            :created_at => 'December 1',
            :updated_at => 'December 1')
User.create(:acct_type => 3,
            :reputation => 0,
            :name => 'user2',
            :username => 'user2',
            :password => 'user2',
            :phone => '123123123',
            :email => 'user2@nku-net.edu',
            :created_at => 'December 1',
            :updated_at => 'December 1')
User.create(:acct_type => 3,
            :reputation => 0,
            :name => 'user3',
            :username => 'user3',
            :password => 'user3',
            :phone => '123123123',
            :email => 'user3@nku-net.edu',
            :created_at => 'December 1',
            :updated_at => 'December 1')

# create some faculty
# email is fct#@nku-net.edu
# pass is fct#
User.create(:acct_type => 2,
            :reputation => 0,
            :name => 'faculty1',
            :username => 'fct1',
            :password => 'fct1',
            :phone => '123123123',
            :email => 'fct1@nku-net.edu',
            :created_at => 'December 1',
            :updated_at => 'December 1')
User.create(:acct_type => 2,
            :reputation => 0,
            :name => 'factulty2',
            :username => 'fct2',
            :password => 'fct2',
            :phone => '123123123',
            :email => 'fct2@nku-net.edu',
            :created_at => 'December 1',
            :updated_at => 'December 1')
User.create(:acct_type => 2,
            :reputation => 0,
            :name => 'faculty3',
            :username => 'fct3',
            :password => 'fct3',
            :phone => '123123123',
            :email => 'fct3@nku-net.edu',
            :created_at => 'December 1',
            :updated_at => 'December 1')
            

Course.create(:name => 'Harambe',
              :code => '420',
              :desc => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenan convallis.',
              :image_url => 'http://aldf.org/wp-content/uploads/2016/06/harambe.jpg',
              :created_at => 'October 20',
              :updated_at => 'October 20')
              
Course.create(:name => 'Pepe',
              :code => '101',
              :desc => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenan convallis.',
              :image_url => 'https://s14.postimg.org/krzpd8zi9/pepe_the_frog.png',
              :created_at => 'October 20',
              :updated_at => 'October 20')

Course.create(:name => 'Grumpy Cat',
              :code => '210',
              :desc => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenan convallis.',
              :image_url => 'http://az616578.vo.msecnd.net/files/2016/05/19/635992707959380360-1327033223_08-grumpy-cat.w1200.h630.jpg',
              :created_at => 'October 20',
              :updated_at => 'October 20')
                                          
Course.create(:name => 'Success',
              :code => '720',
              :desc => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenan convallis.',
              :image_url => 'https://upload.wikimedia.org/wikipedia/en/f/ff/SuccessKid.jpg',
              :created_at => 'October 20',
              :updated_at => 'October 20')
                                          
Course.create(:name => 'Aliens',
              :code => '362',
              :desc => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenan convallis.',
              :image_url => 'https://imgflip.com/s/meme/Ancient-Aliens.jpg',
              :created_at => 'October 20',
              :updated_at => 'October 20')
                                          
Course.create(:name => 'Confession',
              :code => '110',
              :desc => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenan convallis.',
              :image_url => 'https://cdn.meme.am/images/5115240.jpg',
              :created_at => 'October 20',
              :updated_at => 'October 20')
                                          
                                                                                                                                                                        

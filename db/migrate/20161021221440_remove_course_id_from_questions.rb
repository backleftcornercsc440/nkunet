class RemoveCourseIdFromQuestions < ActiveRecord::Migration[5.0]
  def change
    remove_column :questions, :course_id, :int
  end
end

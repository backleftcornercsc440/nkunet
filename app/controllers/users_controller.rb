class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new
    if logged_in? && (current_user.is_admin? || current_user.is_registrar?) 
      @user = User.new
    else
      redirect_to("/")
    end
  end

  # GET /users/1/edit
  def edit
    if logged_in? && (@user == current_user ||  (current_user.is_admin? || current_user.is_registrar?)) 
    
    else
      redirect_to :back
    end
  end

  # POST /users
  # POST /users.json
  def create
    if logged_in? && (current_user.is_admin? || current_user.is_registrar?) 
      power = params[:acct_type].to_i()
      if (power >= current_user.acct_type) 
        @user = User.new(user_params)
        @courses = Course.where(:id => params[:course_list])
        @user.courses << @courses 
        respond_to do |format|
          if @user.save
            format.html { redirect_to @user, notice: 'User was successfully created.' }
            format.json { render :show, status: :created, location: @user }
          else
            format.html { render :new }
            format.json { render json: @user.errors, status: :unprocessable_entity }
          end
        end
      end
    else
      redirect_to :back
    end
    
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    if logged_in? && (current_user.is_admin? || current_user.is_registrar? || current_user == @user)  && current_user.acct_type >= @user.acct_type
      if current_user.is_admin? || current_user.is_registrar?
        @courses = Course.where(:id => params[:course_list])
        @power = params[:acct_type].to_i()
      end
      respond_to do |format|
        if @user.update(user_params)
          if current_user.is_admin? || current_user.is_registrar?
            @user.acct_type = @power
            @user.courses.destroy_all
            @user.courses << @courses 
          end
          @user.save
          format.html { redirect_to @user, notice: 'User was successfully updated.' }
          format.json { render :show, status: :ok, location: @user }
        else
          format.html { render :edit }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to :back
    end
    
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    if logged_in? && (current_user.is_admin? || (current_user.is_registrar? && !@user.is_admin?))
      @user.destroy
      respond_to do |format|
        format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      redirect_to :back, notice: 'You do not have permission to delete that user!'
    end
    
  end
  
  def import
    if logged_in? && (current_user.acct_type == 0 || current_user.acct_type == 1) 
      
    else
      redirect_to :back
    end
  end
  
  def import_file
    if logged_in? && (current_user.acct_type == 0 || current_user.acct_type == 1) 
      myFile = params[:file]
      userCount = User.all.count
      if User.csv_upload(myFile.path)
        userCount = User.all.count - userCount
        redirect_to :users, notice: 'Upload succeded! ' + userCount.to_s() + " new users added!"
      else
        flash[:danger] = 'Upload failed!'
        render 'upload'
      end
      
    else
      redirect_to :back
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:acct_type, :reputation, :name, :username, :password, :phone, :email)
    end
end

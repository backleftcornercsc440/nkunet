class AddQuestionidToVote < ActiveRecord::Migration[5.0]
  def change
    add_column :votes, :question_id, :integer
  end
end

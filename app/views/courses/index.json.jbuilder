json.array!(@courses) do |course|
  json.extract! course, :id, :name, :code, :desc, :image_url
  json.url course_url(course, format: :json)
end

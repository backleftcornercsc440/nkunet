# THIS FILE CONTAINS METHODS THAT CAN BE CALLED FROM ANYWHERE (i think)
# TO FIND OUT THE CURRENT USER, IF ANYONE'S LOGGED IN, AND TO LOGOUT.
class SessionsController < ApplicationController
  def new
  end

  def create
  	user = User.find_by(email: params[:session][:email].downcase)
  	if user && user.authenticate(params[:session][:password])
  		#log in
  		flash[:success] = 'Logged in successfully! Welcome to NkuNet!' # Not quite right!
  		log_in user
  		params[:session][:remember_me] == '1' ? remember(user) : forget(user)
  		redirect_to user
  	else
  		#error message
  		flash[:danger] = 'Invalid email/password combination' # Not quite right!
  		render 'new'
  	end
  end

  def destroy
  	log_out if logged_in?
  	redirect_to :controller => 'welcome', :action => 'index'
  end
end

# README


NKUNet - Verison 1.0 - 10/19/16

-------------------------------
Introduction
-------------------------------

NKUnet is a place where students and teachers can ask / answer questions in regard to NKU.
Blackboard only allows you to communicate with your current classmates. This restricts our
community from speaking with each other and getting answers quickly. 
NKUNet attempts to break down this barrier. 

Bitbucket: https://bitbucket.org/backleftcornercsc440/nkunet
Trello: https://trello.com/backleftcorner
Application: https://nku-net.herokuapp.com/

-------------------------------
Requirements
-------------------------------

Must have a Trello, Bitbutcket, and C9 account.
Originate from the back left corner, obviously. 


-------------------------------
Setup / Configuration
-------------------------------

Navigate to C9.io.

Create new repository.

Clone NKUNet with the following: git clone https://Username@bitbucket.org/backleftcornercsc440/nkunet.git
(Username being your bitbucket username)

-------------------------------
Launching via C9
-------------------------------
When running/testing with C9 use the following command:
rails server -b $IP -p $PORT


-------------------------------
Design and Implementation
-------------------------------

Material Design Lite: https://getmdl.io/

-------------------------------
Goals / Accomplishments
-------------------------------

10/19/2016

1. Procrastinate a little more.
2. Begin the construction of the web app.

10/20/2016

1. Planned database design.
2. Ensure everyone is setup for development.
3. Build scaffolding.
4. Application deployed to heroku.

10/21/2016

1. Constructively browsed dank memes.
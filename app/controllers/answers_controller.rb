class AnswersController < ApplicationController
  before_action :set_answer, only: [:show, :edit, :update, :destroy]

  # GET /answers
  # GET /answers.json
  def index
    @answers = Answer.all
  end

  # GET /answers/1
  # GET /answers/1.json
  def show
  end

  # GET /answers/new
  def new
    if logged_in?
      @answer = Answer.new
    else
      redirect_to :back
    end
    
  end

  # GET /answers/1/edit
  def edit
    if logged_in? && (@answer.user == current_user ||  current_user.is_admin?) 
    
    else
      redirect_to :back
    end
  end

  # POST /answers
  # POST /answers.json
  def create
    
    @course = Course.find(params[:course_id])
    @question = @course.questions.find(params[:question_id])
    @answer = @question.answers.create(answer_params)
    
    if !logged_in?
      flash[:danger] = 'You must be logged in to post questions!' # Not quite right!
      redirect_to course_question_path(@course,@question)
    else
      respond_to do |format|
        @answer.user_id = current_user.id
        if @answer.save
          @answer.up_vote(current_user.id)
          format.html { redirect_to course_question_path(@course,@question), notice: 'Answer was successfully created.' }
          format.json { render :show, status: :created, location: @answer }
        else
          format.html { render :new }
          format.json { render json: @answer.errors, status: :unprocessable_entity }
        end
      end
    end
  end
  # PATCH/PUT /answers/1
  # PATCH/PUT /answers/1.json
  def update
    if logged_in? && (@answer.user == current_user ||  current_user.is_admin?) 
      respond_to do |format|
        if @answer.update(answer_params)
          format.html { redirect_to course_question_path(@answer.question.course,@answer.question), notice: 'Answer was successfully updated.' }
          format.json { render :show, status: :ok, location: @answer }
        else
          format.html { render :edit }
          format.json { render json: @answer.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to :back
    end
    
  end
  
  # POST /questions/X/answers/Y/upvote
  def upvote()
    @course = Course.find(params[:course_id])
    @question = @course.questions.find(params[:question_id])
    if logged_in?
      @answer = @question.answers.find(params[:id])
      @answer.up_vote(current_user.id)
    else
      flash[:danger] = 'You must be logged in to upvote answers!' # Not quite right!
    end
    redirect_to(course_question_path(@question.course,@question))
  end
  
  # POST /questions/X/answers/Y/downvote
  def downvote()
    @course = Course.find(params[:course_id])
    @question = @course.questions.find(params[:question_id])
    if logged_in?
      @answer = @question.answers.find(params[:id])
      @answer.down_vote(current_user.id)
    else
      flash[:danger] = 'You must be logged in to downvote answers!' # Not quite right!
    end
    redirect_to(course_question_path(@question.course,@question))
  end


  # DELETE /answers/1
  # DELETE /answers/1.json
  def destroy
    if logged_in? && (@answer.user == current_user ||  current_user.is_admin?) 
      @answer.destroy
      respond_to do |format|
       format.html { redirect_to course_question_path(@answer.question.course,@answer.question), notice: 'Answer was successfully destroyed.' }
       format.json { head :no_content }
      end
    else
      redirect_to :back
    end
    
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_answer
      @answer = Course.find(params[:course_id]).questions.find(params[:question_id]).answers.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def answer_params
      params.require(:answer).permit(:description, :question_id, :user_id)
    end
end

require 'test_helper'

class AnswerTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  setup do

  	@user = User.create()
  	@user.save
  	@course=Course.create
  	@course.save
  	@question = @user.questions.create!(course_id: @course.id, title: 'hi', description: 'hi')
  	@question.save
  	@answer = @user.answers.create!(question_id: @question.id)
  	@answer.save
  end
  
  test "get answer score (upvote)" do
    assert_equal 0, @answer.get_score
  	@answer.up_vote(1) # userid 1 up votes
  	assert_equal 1, @answer.get_score
  end
  
  test "get answer score (downvote)" do
    assert_equal 0, @answer.get_score
    @answer.down_vote(1) # userid 1 down votes
  	assert_equal -1, @answer.get_score
  end
  
  test "user can't up-vote answers twice" do
    assert_equal 0, @answer.get_score
    @answer.up_vote(1) # userid 1 up votes
  	assert_equal 1, @answer.get_score
  	@answer.up_vote(1) # userid 1 up votes (this should have no impact)
  	assert_equal 1, @answer.get_score
  end
  
  test "user can't down-vote answers twice" do
    assert_equal 0, @answer.get_score
    @answer.down_vote(1) # userid 1 down votes
  	assert_equal -1, @answer.get_score
  	@answer.down_vote(1) # userid 1 down votes (this should have no impact)
  	assert_equal -1, @answer.get_score
  end
  
  test "find user votes test" do
    assert_equal nil, @answer.get_user_vote(1)
    @answer.up_vote(1) # userid 1 up votes
    assert_not_equal nil, @answer.get_user_vote(1)
  end
end

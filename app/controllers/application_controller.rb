class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper
  helper_method :format_users
  
  def format_users(users)
    output = ""
    users.each do |user|
      if output != ""
        output = output + ", "
      end
      output = output + (view_context.link_to user.name, url_for(user))
    end
    if output == ""
       output = "Nobody"
    end
    return output
  end
end
